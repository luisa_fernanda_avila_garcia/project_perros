/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.project;

/**
 *
 * @author ngrajales
 */
public class Nodo {

    private Perro obj_piloto;
    private Nodo nodo_siguiente;
    private Nodo nodo_anterior;

    public Nodo(Perro obj_piloto) {
        this.obj_piloto = obj_piloto;
    }

    public Perro getObj_piloto() {
        return obj_piloto;
    }

    public Nodo getNodo_siguiente() {
        return nodo_siguiente;
    }

    public void setNodo_siguiente(Nodo nodo_siguiente) {
        this.nodo_siguiente = nodo_siguiente;
    }

    public void setObj_piloto(Perro obj_piloto) {
        this.obj_piloto = obj_piloto;
    }

    public Nodo getNodo_anterior() {
        return nodo_anterior;
    }

    public void setNodo_anterior(Nodo nodo_anterior) {
        this.nodo_anterior = nodo_anterior;
    }
    

    @Override
    public String toString() {
        return "\nNodo{" + "obj_piloto=" + obj_piloto + ", nodo_siguiente=" + nodo_siguiente + '}';
    }
    
    

}
